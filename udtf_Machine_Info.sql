-- Return machine information for current partition.

-- Build instructions:

-- *> RUNSQLSTM SRCSTMF('&FP') COMMIT(*NONE) DFTRDBCOL(&FCN2) <*

create or replace function Machine_Info ()
  returns table(
                 Maximum_memory                                                  bigint
               , Minimum_memory                                                  bigint
               , Dispatch_wheel_rotation_period                                  bigint
               , Partition_ID                                                    integer
               , Scaled_processor_time                                           varchar(   3 )
               , Bound_hardware_threads                                          varchar(   3 )
               , Dedicated_processors                                            varchar(   3 )
               , Maximum_processors_in_physical_machine                          integer
               , Minimum_virtual_processors                                      integer
               , Maximum_virtual_processors                                      integer
               , Minimum_processing_capacity                                     integer
               , Maximum_processing_capacity                                     integer
               , Processing_capacity_delta                                       integer
               , Minimum_interactive_capacity_percentage                         integer
               , Maximum_interactive_capacity_percentage                         integer
               , Hardware_threads_per_processor                                  smallint
               , Partition_name                                                  varchar( 255 )
               , Type_of_5250_measurement                                        varchar(   4 )
               , Memory_delta                                                    bigint
               , Configured_virtual_processors                                   integer
               , Configured_processing_capacity                                  integer
               , Configured_interactive_capacity_percentage                      integer
               , Configured_variable_processing_capacity_weight                  integer
               , Configured_memory                                               bigint
               , Minimum_5250_OLTP_user_authorizations                           integer
               , Maximum_5250_OLTP_user_authorizations                           integer
               , Configured_5250_OLTP_user_authorizations                        integer
               , Usable_memory                                                   bigint
               , CPU_time_since_IPL                                              bigint
               , Interactive_time_since_IPL                                      bigint
               , Excess_interactive_time_since_IPL                               bigint
               , Shared_processor_pool_idle_time_since_IPL                       bigint
               , Service_aggregation_point                                       varchar(   3 )
               , Capped_partition                                                varchar(   3 )
               , Hardware_multithreading                                         varchar(   3 )
               , Shared_processor_pool_idle_time_since_IPL_valid                 varchar(   3 )
               , Processors_in_the_physical_machine                              integer
               , Usable_virtual_processors                                       integer
               , Physical_processors_in_the_shared_processor_pool                integer
               , Unallocated_processor_capacity_in_partition_group               integer
               , Usable_processing_capacity                                      integer
               , Usable_variable_processing_capacity_weight                      integer
               , Unallocated_variable_processing_capacity_weight                 integer
               , Minimum_required_processing_capacity                            integer
               , Interactive_capacity_percentage                                 integer
               , Partition_group_ID                                              smallint
               , Shared_processor_pool_ID                                        smallint
               , Interactive_threshold                                           smallint
               , Unallocated_interactive_processor_capacity_in_partition_group   integer
               , Scaled_CPU_time_since_IPL                                       bigint
               , Usable_5250_OLTP_user_authorization                             integer
               , Unallocated_5250_OLTP_user_authorization_in_partition_group     integer
               , Active_5250_users                                               bigint
  )
external name 'QGPL/MACHINF(Machine_Info)'
specific MACHINE_INFO
language RPGLE
not deterministic
no sql
parameter style SQL
no final call
;
