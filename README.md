# SQL UDTF Machine_Info #

## What is this repository for? ##

The SQL table function will return information about the current IBM i partition, e.g. partition ID and name.

The following attributes are returned:

Attribute                                                     | Data type                     | Description
--------------------------------------------------------------|:------------------------------|:------------------------------------------
Maximum_memory                                                | bigint                        | The maximum amount of memory (in units of megabytes) that can be assigned to this partition.
Minimum_memory                                                | bigint                        | The minimum amount of memory (in units of megabytes) that is needed in this partition.
Dispatch_wheel_rotation_period                                | bigint                        | The number of nanoseconds in the hypervisor's scheduling window.
Partition_ID                                                  | integer                       | The identifier of this partition.
Scaled_processor_time                                         | varchar(3)                    | YES or NO. Whether processor time may or may not be scaled during the current IPL.
Bound_hardware_threads                                        | varchar(3)                    | YES or NO. Whether or not hardware threads are bound.
Dedicated_processors                                          | varchar(3)                    | YES or NO. Whether or not the partition uses only dedicated physical processors.
Maximum_processors_in_physical_machine                        | integer                       | The maximum number of physical processors that can be active in this physical machine without physically installing additional processors.
Minimum_virtual_processors                                    | integer                       | The minimum number of virtual processors that are needed in this partition.
Maximum_virtual_processors                                    | integer                       | The maximum number of virtual processors that can be assigned to this partition.
Minimum_processing_capacity                                   | integer                       | The minimum amount of processing capacity (in units of 1/100 of a physical processor) that is needed in this partition.
Maximum_processing_capacity                                   | integer                       | The maximum amount of processing capacity (in units of 1/100 of a physical processor) that can be assigned to this partition.
Processing_capacity_delta                                     | integer                       | The delta (in units of 1/100 of a physical processor) that can be added to or removed from this partition's processing capacity.
Minimum_interactive_capacity_percentage                       | integer                       | The minimum value that can be set for this partition's interactive capacity percentage.
Maximum_interactive_capacity_percentage                       | integer                       | The maximum value that can be set for this partition's interactive capacity percentage.
Hardware_threads_per_processor                                | smallint                      | The number of hardware threads per processor when hardware multi-threading is enabled.
Partition_name                                                | varchar(255)                  | The name that has been assigned to this partition.
Type_of_5250_measurement                                      | varchar(4)                    | CPW, USER or UNKNOWN. Indicates the 5250 type of measurement of the physical machine.
Memory_delta                                                  | bigint                        | The delta (in units of megabytes) that can be added to or removed from this partition's memory.
Configured_virtual_processors                                 | integer                       | The number of virtual processors that are configured for this partition.
Configured_processing_capacity                                | integer                       | The processing capacity (in units of 1/100 of a physical processor) that is configured for this partition.
Configured_interactive_capacity_percentage                    | integer                       | The interactive processing capacity that is configured for this partition.
Configured_variable_processing_capacity_weight                | integer                       | The variable processing capacity weight that is configured for this partition.
Configured_memory                                             | bigint                        | The physical memory size (in units of megabytes) that is configured for this partition.
Minimum_5250_OLTP_user_authorizations                         | integer                       | The minimum value that are needed for this partition.
Maximum_5250_OLTP_user_authorizations                         | integer                       | The maximum value that can set for this partition.
Configured_5250_OLTP_user_authorizations                      | integer                       | The value configured for this partition from HMC.
Usable_memory                                                 | bigint                        | The amount of usable memory (in units of megabytes) currently allocated to this partition.
CPU_time_since_IPL                                            | bigint                        | The number of nanoseconds of processor time used since IPL.
Interactive_time_since_IPL                                    | bigint                        | The number of nanoseconds of processor time used by interactive processes since IPL.
Excess_interactive_time_since_IPL                             | bigint                        | The number of nanoseconds of processor time used by interactive processes since IPL, that exceeded interactive capacity.
Shared_processor_pool_idle_time_since_IPL                     | bigint                        | The number of nanoseconds of processor time that the shared processor pool has been idle since IPL, when shared processor pool idle time since IPL was materialized.
Service_aggregation_point                                     | varchar(3)                    | YES or NO. Whether or not this partition has a service aggregation point.
Capped_partition                                              | varchar(3)                    | YES or NO. Whether or not the partition is allowed to use more than its processing capacity.
Hardware_multithreading                                       | varchar(3)                    | YES or NO. Whether or not hardware multi-threading is enabled.
Shared_processor_pool_idle_time_since_IPL_valid               | varchar(3)                    | YES or NO. Whether or not the shared processor pool idle time since IPL was successfully materialized.
Processors_in_the_physical_machine                            | integer                       | The number of physical processors in this physical machine that are available for customer use.
Usable_virtual_processors                                     | integer                       | The number of usable virtual processors in this partition.
Physical_processors_in_the_shared_processor_pool              | integer                       | The number of physical processors that are allocated to the shared processor pool in which this partition is executing.
Unallocated_processor_capacity_in_partition_group             | integer                       | The amount of processing capacity (in units of 1/100 of a physical processor) in this partition's group that is available to be allocated to processing capacity.
Usable_processing_capacity                                    | integer                       | The amount of processing capacity (in units of 1/100 of a physical processor) currently available to the partition.
Usable_variable_processing_capacity_weight                    | integer                       | The weighting factor that is used to assign additional unused CPU cycles (from the shared processor pool) to the partition.
Unallocated_variable_processing_capacity_weight               | integer                       | The amount of capacity weight that is available for allocation to the variable processing capacity weight.
Minimum_required_processing_capacity                          | integer                       | The amount of processing capacity (in units of 1/100 of a physical processor) that the operating system requires in this partition.
Interactive_capacity_percentage                               | integer                       | This partition's portion (in hundredths of a percent) of the physical machine's interactive capacity.
Partition_group_ID                                            | smallint                      | The LPAR group that this partition is a member of.
Shared_processor_pool_ID                                      | smallint                      | The shared processor pool this partition is a member of.
Interactive_threshold                                         | smallint                      | The maximum interactive processor utilization (in hundredths of a percent) which can be sustained in this partition, without causing a disproportionate increase in system overhead.
Unallocated_interactive_processor_capacity_in_partition_group | integer                       | The portion (in hundredths of percent) of the physical machine's interactive capacity that is available to be allocated to *interactive capacity percentage* of partitions in this partition's group.
Scaled_CPU_time_since_IPL                                     | bigint                        | The number of nonoseconds of scaled processor utilized time used since IPL.
Usable_5250_OLTP_user_authorization                           | integer                       | The number of 5250 OLTP user authorizations that the partition has allocated from the total 5250 OLTP user authorizations in the physical machine.
Unallocated_5250_OLTP_user_authorization_in_partition_group   | integer                       | The number of 5250 OLTP user authorizations in the physical machine that is available to be allocated to the usable 5250 OLTP user authorizations of partitions in the partition's group.
Active_5250_users                                             | bigint                        | The number of 5250 users currently active.



## How do I get set up? ##

Clone this git repository to a local directory in the IFS, e.g. in your home directory.
Compile the source using the following CL commands (objects will be placed in the QGPL library):

```
CRTRPGMOD MODULE(QGPL/MACHINF) SRCSTMF('machinf.rpgle')
CRTSRVPGM SRVPGM(QGPL/MACHINF) EXPORT(*ALL) TEXT('Machine_Info')
DLTMOD    MODULE(QGPL/MACHINF)
RUNSQLSTM SRCSTMF('udtf_Machine_Info.sql') DFTRDBCOL(QGPL)
```

Call the SQL function like the following
```
select * from table( qgpl.machine_info() )
```



## Documentation ##

[ MI API Materialize Machine Information (MATMIF)](https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_73/rzatk/MATMIF.htm)