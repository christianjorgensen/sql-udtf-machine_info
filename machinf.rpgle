**free
/if not defined( Copying_prototypes )
//-----------------------------------------------------------------------------------------------------------/
//                                                                                                           /
//  Brief description of collection of procedures.                                                           /
//                                                                                                           /
//  Procedures:                                                                                              /
//                                                                                                           /
//  Machine_Info          - return info about machine information for current partition.                     /
//                                                                                                           /
//  Compilation:                                                                                             /
//                                                                                                           /
//*>  CRTRPGMOD  MODULE(&FCN2/&FNR) SRCSTMF('&FP')                                                         <*/
//*>  CRTSRVPGM  SRVPGM(&FCN2/&FNR) EXPORT(*ALL) TEXT('Machine_Info')                                      <*/
//*>  DLTMOD     MODULE(&FCN2/&FNR)                                                                        <*/
//                                                                                                           /
//-----------------------------------------------------------------------------------------------------------/
//  2019-07-04 : Christian Jorgensen                                                                         /
//               Create module.                                                                              /
//  2019-07-16 : Christian Jorgensen                                                                         /
//               Fix error with returning correct Type_of_5250_measurement.                                  /
//  2020-02-05 : Christian Jorgensen                                                                         /
//               Fix wrong definition of SQL parameter 'CallType'.                                           /
//-----------------------------------------------------------------------------------------------------------/

ctl-opt debug;
ctl-opt option( *Srcstmt : *NoDebugIO );
ctl-opt thread( *serialize );

ctl-opt nomain;

//-----------------------------------------------------------------------------------------------------------/
// Exported procedures:

/endif

/if not defined( MACHINF_prototype_copied )

// Procedure:    Machine_Info
// Description:  Return info about machine information for current partition.

dcl-pr Machine_Info extproc( *dclcase );
  // Returned parameters
  p_Maximum_memory                                                  uns( 20 );
  p_Minimum_memory                                                  uns( 20 );
  p_Dispatch_wheel_rotation_period                                  uns( 20 );
  p_Partition_ID                                                    uns( 10 );
  p_Scaled_processor_time                                           varchar(   3 );
  p_Bound_hardware_threads                                          varchar(   3 );
  p_Dedicated_processors                                            varchar(   3 );
  p_Maximum_processors_in_physical_machine                          uns( 10 );
  p_Minimum_virtual_processors                                      uns( 10 );
  p_Maximum_virtual_processors                                      uns( 10 );
  p_Minimum_processing_capacity                                     uns( 10 );
  p_Maximum_processing_capacity                                     uns( 10 );
  p_Processing_capacity_delta                                       uns( 10 );
  p_Minimum_interactive_capacity_percentage                         uns( 10 );
  p_Maximum_interactive_capacity_percentage                         uns( 10 );
  p_Hardware_threads_per_processor                                  uns(  5 );
  p_Partition_name                                                  varchar( 255 );
  p_Type_of_5250_measurement                                        varchar(   4 );
  p_Memory_delta                                                    uns( 20 );
  p_Configured_virtual_processors                                   uns( 10 );
  p_Configured_processing_capacity                                  uns( 10 );
  p_Configured_interactive_capacity_percentage                      uns( 10 );
  p_Configured_variable_processing_capacity_weight                  uns( 10 );
  p_Configured_memory                                               uns( 20 );
  p_Minimum_5250_OLTP_user_authorizations                           uns( 10 );
  p_Maximum_5250_OLTP_user_authorizations                           uns( 10 );
  p_Configured_5250_OLTP_user_authorizations                        uns( 10 );
  p_Usable_memory                                                   uns( 20 );
  p_CPU_time_since_IPL                                              uns( 20 );
  p_Interactive_time_since_IPL                                      uns( 20 );
  p_Excess_interactive_time_since_IPL                               uns( 20 );
  p_Shared_processor_pool_idle_time_since_IPL                       uns( 20 );
  p_Service_aggregation_point                                       varchar(   3 );
  p_Capped_partition                                                varchar(   3 );
  p_Hardware_multithreading                                         varchar(   3 );
  p_Shared_processor_pool_idle_time_since_IPL_valid                 varchar(   3 );
  p_Processors_in_the_physical_machine                              uns( 10 );
  p_Usable_virtual_processors                                       uns( 10 );
  p_Physical_processors_in_the_shared_processor_pool                uns( 10 );
  p_Unallocated_processor_capacity_in_partition_group               uns( 10 );
  p_Usable_processing_capacity                                      uns( 10 );
  p_Usable_variable_processing_capacity_weight                      uns( 10 );
  p_Unallocated_variable_processing_capacity_weight                 uns( 10 );
  p_Minimum_required_processing_capacity                            uns( 10 );
  p_Interactive_capacity_percentage                                 uns( 10 );
  p_Partition_group_ID                                              uns(  5 );
  p_Shared_processor_pool_ID                                        uns(  5 );
  p_Interactive_threshold                                           uns(  5 );
  p_Unallocated_interactive_processor_capacity_in_partition_group   int( 10 );
  p_Scaled_CPU_time_since_IPL                                       uns( 20 );
  p_Usable_5250_OLTP_user_authorization                             int( 10 );
  p_Unallocated_5250_OLTP_user_authorization_in_partition_group     int( 10 );
  p_Active_5250_users                                               int( 20 );
  // Null indicators for returned parameters
  n_Maximum_memory                                                  int(  5 );
  n_Minimum_memory                                                  int(  5 );
  n_Dispatch_wheel_rotation_period                                  int(  5 );
  n_Partition_ID                                                    int(  5 );
  n_Scaled_processor_time                                           int(  5 );
  n_Bound_hardware_threads                                          int(  5 );
  n_Dedicated_processors                                            int(  5 );
  n_Maximum_processors_in_physical_machine                          int(  5 );
  n_Minimum_virtual_processors                                      int(  5 );
  n_Maximum_virtual_processors                                      int(  5 );
  n_Minimum_processing_capacity                                     int(  5 );
  n_Maximum_processing_capacity                                     int(  5 );
  n_Processing_capacity_delta                                       int(  5 );
  n_Minimum_interactive_capacity_percentage                         int(  5 );
  n_Maximum_interactive_capacity_percentage                         int(  5 );
  n_Hardware_threads_per_processor                                  int(  5 );
  n_Partition_name                                                  int(  5 );
  n_5250_measurement_type                                           int(  5 );
  n_Memory_delta                                                    int(  5 );
  n_Configured_virtual_processors                                   int(  5 );
  n_Configured_processing_capacity                                  int(  5 );
  n_Configured_interactive_capacity_percentage                      int(  5 );
  n_Configured_variable_processing_capacity_weight                  int(  5 );
  n_Configured_memory                                               int(  5 );
  n_Minimum_5250_OLTP_user_authorizations                           int(  5 );
  n_Maximum_5250_OLTP_user_authorizations                           int(  5 );
  n_Configured_5250_OLTP_user_authorizations                        int(  5 );
  n_Usable_memory                                                   int(  5 );
  n_CPU_time_since_IPL                                              int(  5 );
  n_Interactive_time_since_IPL                                      int(  5 );
  n_Excess_interactive_time_since_IPL                               int(  5 );
  n_Shared_processor_pool_idle_time_since_IPL                       int(  5 );
  n_Service_aggregation_point                                       int(  5 );
  n_Capped_partition                                                int(  5 );
  n_Hardware_multithreading                                         int(  5 );
  n_Shared_processor_pool_idle_time_since_IPL_valid                 int(  5 );
  n_Processors_in_the_physical_machine                              int(  5 );
  n_Usable_virtual_processors                                       int(  5 );
  n_Physical_processors_in_the_shared_processor_pool                int(  5 );
  n_Unallocated_processor_capacity_in_partition_group               int(  5 );
  n_Usable_processing_capacity                                      int(  5 );
  n_Usable_variable_processing_capacity_weight                      int(  5 );
  n_Unallocated_variable_processing_capacity_weight                 int(  5 );
  n_Minimum_required_processing_capacity                            int(  5 );
  n_Interactive_capacity_percentage                                 int(  5 );
  n_Partition_group_ID                                              int(  5 );
  n_Shared_processor_pool_ID                                        int(  5 );
  n_Interactive_threshold                                           int(  5 );
  n_Unallocated_interactive_processor_capacity_in_partition_group   int(  5 );
  n_Scaled_CPU_time_since_IPL                                       int(  5 );
  n_Usable_5250_OLTP_user_authorization                             int(  5 );
  n_Unallocated_5250_OLTP_user_authorization_in_partition_group     int(  5 );
  n_Active_5250_users                                               int(  5 );
  // SQL parameters.
  Sql_State                                                         char( 5 );
  Function                                                          varchar(  517 ) const;
  Specific                                                          varchar(  128 ) const;
  MsgText                                                           varchar( 1000 );
  CallType                                                          int( 10 )       const;
end-pr;

/define MACHINF_prototype_copied
/if defined( Copying_prototypes )
/eof

/endif
/endif

//----------------------------------------------------------------
// Constants:

dcl-c BIT_SCALED_PROCESSOR_TIME                             x'00000004';      // 00000000 00000000 00000000 00000100
dcl-c BIT_BOUND_HARDWARE_THREADS                            x'00000002';      // 00000000 00000000 00000000 00000010
dcl-c BIT_DEDICATED_PROCESSORS                              x'00000001';      // 00000000 00000000 00000000 00000001
dcl-c BIT_SERVICE_AGGREGATION_POINT                         x'00000008';      // 00000000 00000000 00000000 00001000
dcl-c BIT_CAPPED_PARTITION                                  x'00000004';      // 00000000 00000000 00000000 00000100
dcl-c BIT_HARDWARE_MULTITHREADING                           x'00000002';      // 00000000 00000000 00000000 00000010
dcl-c BIT_SHARED_PROCESSOR_POOL_IDLE_TIME_SINCE_IPL_VALID   x'00000001';      // 00000000 00000000 00000000 00000001

dcl-c CALL_STARTUP    -2;
dcl-c CALL_OPEN       -1;
dcl-c CALL_FETCH       0;
dcl-c CALL_CLOSE       1;
dcl-c CALL_FINAL       2;

dcl-c PARM_NULL       -1;
dcl-c PARM_NOTNULL     0;

//----------------------------------------------------------------
// Data types:

dcl-ds MATMIF_0001_t template qualified align( *full );
  Bytes_provided                                                    int( 10 )  inz( %size( MATMIF_0001_t ) );
  Bytes_available                                                   int( 10 );
  Maximum_memory                                                    uns( 20 );
  Minimum_memory                                                    uns( 20 );
  Dispatch_wheel_rotation_period                                    uns( 20 );
  Partition_ID                                                      uns( 10 );
  Indicators                                                        char( 4 );
  Maximum_processors_in_physical_machine                            uns( 10 );
  Minimum_virtual_processors                                        uns( 10 );
  Maximum_virtual_processors                                        uns( 10 );
  Minimum_processing_capacity                                       uns( 10 );
  Maximum_processing_capacity                                       uns( 10 );
  Processing_capacity_delta                                         uns( 10 );
  Minimum_interactive_capacity_percentage                           uns( 10 );
  Maximum_interactive_capacity_percentage                           uns( 10 );
  Hardware_threads_per_processor                                    uns(  5 );
  Partition_name                                                    char( 256 ) ccsid( 819 );
  *n                                                                char( 5 );
  Type_of_5250_measurement                                          uns(  3 );
  Memory_delta                                                      uns( 20 );
  Configured_virtual_processors                                     uns( 10 );
  Configured_processing_capacity                                    uns( 10 );
  Configured_interactive_capacity_percentage                        uns( 10 );
  Configured_variable_processing_capacity_weight                    uns( 10 );
  Configured_memory                                                 uns( 20 );
  Minimum_5250_OLTP_user_authorizations                             int( 10 );
  Maximum_5250_OLTP_user_authorizations                             int( 10 );
  Configured_5250_OLTP_user_authorizations                          int( 10 );
end-ds;

dcl-ds MATMIF_0002_t template qualified align( *full );
  Bytes_provided                                                    int( 10 )  inz( %size( MATMIF_0002_t ) );
  Bytes_available                                                   int( 10 );
  Usable_memory                                                     uns( 20 );
  CPU_time_since_IPL                                                uns( 20 );
  Interactive_time_since_IPL                                        uns( 20 );
  Excess_interactive_time_since_IPL                                 uns( 20 );
  Shared_processor_pool_idle_time_since_IPL                         uns( 20 );
  Indicators                                                        char( 4 );
  Processors_in_the_physical_machine                                uns( 10 );
  Usable_virtual_processors                                         uns( 10 );
  Physical_processors_in_the_shared_processor_pool                  uns( 10 );
  Unallocated_processor_capacity_in_partition_group                 uns( 10 );
  Usable_processing_capacity                                        uns( 10 );
  Usable_variable_processing_capacity_weight                        uns( 10 );
  Unallocated_variable_processing_capacity_weight                   uns( 10 );
  Minimum_required_processing_capacity                              uns( 10 );
  Interactive_capacity_percentage                                   uns( 10 );
  Partition_group_ID                                                uns(  5 );
  Shared_processor_pool_ID                                          uns(  5 );
  Interactive_threshold                                             uns(  5 );
  *n                                                                char( 1 );
  Type_of_5250_measurement                                          uns(  3 );
  Unallocated_interactive_processor_capacity_in_partition_group     int( 10 );
  Scaled_CPU_time_since_IPL                                         uns( 20 );
  *n                                                                char( 4 );
  Usable_5250_OLTP_user_authorization                               int( 10 );
  Unallocated_5250_OLTP_user_authorization_in_partition_group       int( 10 );
  Active_5250_users                                                 int( 20 );
end-ds;

// API error data structure:

dcl-ds ApiError_t template qualified;
  BytPrv  int( 10 )   inz( %size( ApiError_t ) );
  BytAvl  int( 10 );
  ExcpId  char(   7 );
  *n      char(   1 );
  ExcpDta char( 128 );
end-ds;

//----------------------------------------------------------------
// Procedure:    Machine_Info
// Description:  Return info about machine information for current partition.

dcl-proc Machine_Info export;

  dcl-pi *n;
    // Returned parameters
    p_Maximum_memory                                                  uns( 20 );
    p_Minimum_memory                                                  uns( 20 );
    p_Dispatch_wheel_rotation_period                                  uns( 20 );
    p_Partition_ID                                                    uns( 10 );
    p_Scaled_processor_time                                           varchar(   3 );
    p_Bound_hardware_threads                                          varchar(   3 );
    p_Dedicated_processors                                            varchar(   3 );
    p_Maximum_processors_in_physical_machine                          uns( 10 );
    p_Minimum_virtual_processors                                      uns( 10 );
    p_Maximum_virtual_processors                                      uns( 10 );
    p_Minimum_processing_capacity                                     uns( 10 );
    p_Maximum_processing_capacity                                     uns( 10 );
    p_Processing_capacity_delta                                       uns( 10 );
    p_Minimum_interactive_capacity_percentage                         uns( 10 );
    p_Maximum_interactive_capacity_percentage                         uns( 10 );
    p_Hardware_threads_per_processor                                  uns(  5 );
    p_Partition_name                                                  varchar( 255 );
    p_Type_of_5250_measurement                                        varchar(   4 );
    p_Memory_delta                                                    uns( 20 );
    p_Configured_virtual_processors                                   uns( 10 );
    p_Configured_processing_capacity                                  uns( 10 );
    p_Configured_interactive_capacity_percentage                      uns( 10 );
    p_Configured_variable_processing_capacity_weight                  uns( 10 );
    p_Configured_memory                                               uns( 20 );
    p_Minimum_5250_OLTP_user_authorizations                           uns( 10 );
    p_Maximum_5250_OLTP_user_authorizations                           uns( 10 );
    p_Configured_5250_OLTP_user_authorizations                        uns( 10 );
    p_Usable_memory                                                   uns( 20 );
    p_CPU_time_since_IPL                                              uns( 20 );
    p_Interactive_time_since_IPL                                      uns( 20 );
    p_Excess_interactive_time_since_IPL                               uns( 20 );
    p_Shared_processor_pool_idle_time_since_IPL                       uns( 20 );
    p_Service_aggregation_point                                       varchar(   3 );
    p_Capped_partition                                                varchar(   3 );
    p_Hardware_multithreading                                         varchar(   3 );
    p_Shared_processor_pool_idle_time_since_IPL_valid                 varchar(   3 );
    p_Processors_in_the_physical_machine                              uns( 10 );
    p_Usable_virtual_processors                                       uns( 10 );
    p_Physical_processors_in_the_shared_processor_pool                uns( 10 );
    p_Unallocated_processor_capacity_in_partition_group               uns( 10 );
    p_Usable_processing_capacity                                      uns( 10 );
    p_Usable_variable_processing_capacity_weight                      uns( 10 );
    p_Unallocated_variable_processing_capacity_weight                 uns( 10 );
    p_Minimum_required_processing_capacity                            uns( 10 );
    p_Interactive_capacity_percentage                                 uns( 10 );
    p_Partition_group_ID                                              uns(  5 );
    p_Shared_processor_pool_ID                                        uns(  5 );
    p_Interactive_threshold                                           uns(  5 );
    p_Unallocated_interactive_processor_capacity_in_partition_group   int( 10 );
    p_Scaled_CPU_time_since_IPL                                       uns( 20 );
    p_Usable_5250_OLTP_user_authorization                             int( 10 );
    p_Unallocated_5250_OLTP_user_authorization_in_partition_group     int( 10 );
    p_Active_5250_users                                               int( 20 );
    // Null indicators for returned parameters
    n_Maximum_memory                                                  int(  5 );
    n_Minimum_memory                                                  int(  5 );
    n_Dispatch_wheel_rotation_period                                  int(  5 );
    n_Partition_ID                                                    int(  5 );
    n_Scaled_processor_time                                           int(  5 );
    n_Bound_hardware_threads                                          int(  5 );
    n_Dedicated_processors                                            int(  5 );
    n_Maximum_processors_in_physical_machine                          int(  5 );
    n_Minimum_virtual_processors                                      int(  5 );
    n_Maximum_virtual_processors                                      int(  5 );
    n_Minimum_processing_capacity                                     int(  5 );
    n_Maximum_processing_capacity                                     int(  5 );
    n_Processing_capacity_delta                                       int(  5 );
    n_Minimum_interactive_capacity_percentage                         int(  5 );
    n_Maximum_interactive_capacity_percentage                         int(  5 );
    n_Hardware_threads_per_processor                                  int(  5 );
    n_Partition_name                                                  int(  5 );
    n_5250_measurement_type                                           int(  5 );
    n_Memory_delta                                                    int(  5 );
    n_Configured_virtual_processors                                   int(  5 );
    n_Configured_processing_capacity                                  int(  5 );
    n_Configured_interactive_capacity_percentage                      int(  5 );
    n_Configured_variable_processing_capacity_weight                  int(  5 );
    n_Configured_memory                                               int(  5 );
    n_Minimum_5250_OLTP_user_authorizations                           int(  5 );
    n_Maximum_5250_OLTP_user_authorizations                           int(  5 );
    n_Configured_5250_OLTP_user_authorizations                        int(  5 );
    n_Usable_memory                                                   int(  5 );
    n_CPU_time_since_IPL                                              int(  5 );
    n_Interactive_time_since_IPL                                      int(  5 );
    n_Excess_interactive_time_since_IPL                               int(  5 );
    n_Shared_processor_pool_idle_time_since_IPL                       int(  5 );
    n_Service_aggregation_point                                       int(  5 );
    n_Capped_partition                                                int(  5 );
    n_Hardware_multithreading                                         int(  5 );
    n_Shared_processor_pool_idle_time_since_IPL_valid                 int(  5 );
    n_Processors_in_the_physical_machine                              int(  5 );
    n_Usable_virtual_processors                                       int(  5 );
    n_Physical_processors_in_the_shared_processor_pool                int(  5 );
    n_Unallocated_processor_capacity_in_partition_group               int(  5 );
    n_Usable_processing_capacity                                      int(  5 );
    n_Usable_variable_processing_capacity_weight                      int(  5 );
    n_Unallocated_variable_processing_capacity_weight                 int(  5 );
    n_Minimum_required_processing_capacity                            int(  5 );
    n_Interactive_capacity_percentage                                 int(  5 );
    n_Partition_group_ID                                              int(  5 );
    n_Shared_processor_pool_ID                                        int(  5 );
    n_Interactive_threshold                                           int(  5 );
    n_Unallocated_interactive_processor_capacity_in_partition_group   int(  5 );
    n_Scaled_CPU_time_since_IPL                                       int(  5 );
    n_Usable_5250_OLTP_user_authorization                             int(  5 );
    n_Unallocated_5250_OLTP_user_authorization_in_partition_group     int(  5 );
    n_Active_5250_users                                               int(  5 );
    // SQL parameters.
    Sql_State                                                         char( 5 );
    Function                                                          varchar(  517 ) const;
    Specific                                                          varchar(  128 ) const;
    MsgText                                                           varchar( 1000 );
    CallType                                                          int( 10 )       const;
  end-pi;

  dcl-pr Materialize_Machine_Information extproc( '_MATMIF' );
    *n likeds( MATMIF_0001_t ) options( *varsize );
    *n uns( 5 ) value;
  end-pr;

  dcl-s  SaveCallType          like( CallType ) static;  // Save CallType from previous call...

  dcl-ds MATMIF_0001_info      likeds( MATMIF_0001_t );
  dcl-ds MATMIF_0002_info      likeds( MATMIF_0002_t );

  dcl-c  MATMIF_OPTION_1       1;
  dcl-c  MATMIF_OPTION_2       2;

  dcl-s  Partition_name_temp   char( 256 );

  dcl-ds ApiError              likeds( ApiError_t );

  // Global procedure monitor.

  monitor;

    // Start all fields at not NULL.

    n_Maximum_memory                                                  = PARM_NOTNULL;
    n_Minimum_memory                                                  = PARM_NOTNULL;
    n_Dispatch_wheel_rotation_period                                  = PARM_NOTNULL;
    n_Partition_ID                                                    = PARM_NOTNULL;
    n_Scaled_processor_time                                           = PARM_NOTNULL;
    n_Bound_hardware_threads                                          = PARM_NOTNULL;
    n_Dedicated_processors                                            = PARM_NOTNULL;
    n_Maximum_processors_in_physical_machine                          = PARM_NOTNULL;
    n_Minimum_virtual_processors                                      = PARM_NOTNULL;
    n_Maximum_virtual_processors                                      = PARM_NOTNULL;
    n_Minimum_processing_capacity                                     = PARM_NOTNULL;
    n_Maximum_processing_capacity                                     = PARM_NOTNULL;
    n_Processing_capacity_delta                                       = PARM_NOTNULL;
    n_Minimum_interactive_capacity_percentage                         = PARM_NOTNULL;
    n_Maximum_interactive_capacity_percentage                         = PARM_NOTNULL;
    n_Hardware_threads_per_processor                                  = PARM_NOTNULL;
    n_Partition_name                                                  = PARM_NOTNULL;
    n_5250_measurement_type                                           = PARM_NOTNULL;
    n_Memory_delta                                                    = PARM_NOTNULL;
    n_Configured_virtual_processors                                   = PARM_NOTNULL;
    n_Configured_processing_capacity                                  = PARM_NOTNULL;
    n_Configured_interactive_capacity_percentage                      = PARM_NOTNULL;
    n_Configured_variable_processing_capacity_weight                  = PARM_NOTNULL;
    n_Configured_memory                                               = PARM_NOTNULL;
    n_Minimum_5250_OLTP_user_authorizations                           = PARM_NOTNULL;
    n_Maximum_5250_OLTP_user_authorizations                           = PARM_NOTNULL;
    n_Configured_5250_OLTP_user_authorizations                        = PARM_NOTNULL;
    n_Usable_memory                                                   = PARM_NOTNULL;
    n_CPU_time_since_IPL                                              = PARM_NOTNULL;
    n_Interactive_time_since_IPL                                      = PARM_NOTNULL;
    n_Excess_interactive_time_since_IPL                               = PARM_NOTNULL;
    n_Shared_processor_pool_idle_time_since_IPL                       = PARM_NOTNULL;
    n_Usable_memory                                                   = PARM_NOTNULL;
    n_Usable_memory                                                   = PARM_NOTNULL;
    n_Usable_memory                                                   = PARM_NOTNULL;
    n_Service_aggregation_point                                       = PARM_NOTNULL;
    n_Capped_partition                                                = PARM_NOTNULL;
    n_Hardware_multithreading                                         = PARM_NOTNULL;
    n_Shared_processor_pool_idle_time_since_IPL_valid                 = PARM_NOTNULL;
    n_Processors_in_the_physical_machine                              = PARM_NOTNULL;
    n_Usable_virtual_processors                                       = PARM_NOTNULL;
    n_Physical_processors_in_the_shared_processor_pool                = PARM_NOTNULL;
    n_Unallocated_processor_capacity_in_partition_group               = PARM_NOTNULL;
    n_Usable_processing_capacity                                      = PARM_NOTNULL;
    n_Usable_variable_processing_capacity_weight                      = PARM_NOTNULL;
    n_Minimum_required_processing_capacity                            = PARM_NOTNULL;
    n_Interactive_capacity_percentage                                 = PARM_NOTNULL;
    n_Partition_group_ID                                              = PARM_NOTNULL;
    n_Shared_processor_pool_ID                                        = PARM_NOTNULL;
    n_Interactive_threshold                                           = PARM_NOTNULL;
    n_Unallocated_interactive_processor_capacity_in_partition_group   = PARM_NOTNULL;

    //  Open, fetch & close...

    select;
      when  CallType = CALL_FETCH;

        // If previous call was for fetch, return EOF.

        if ( CallType = SaveCallType );
          SQL_State = '02000';
          return;
        endif;

        // Get machine information.

        Materialize_Machine_Information( MATMIF_0001_info : MATMIF_OPTION_1 );

        p_Maximum_memory                                                  = MATMIF_0001_info.Maximum_memory                                               ;
        p_Minimum_memory                                                  = MATMIF_0001_info.Minimum_memory                                               ;
        p_Dispatch_wheel_rotation_period                                  = MATMIF_0001_info.Dispatch_wheel_rotation_period                               ;
        p_Partition_ID                                                    = MATMIF_0001_info.Partition_ID                                                 ;

        if ( %bitand( MATMIF_0001_info.Indicators : BIT_SCALED_PROCESSOR_TIME ) = BIT_SCALED_PROCESSOR_TIME );
          p_Scaled_processor_time = 'YES';
        else;
          p_Scaled_processor_time = 'NO';
        endif;

        if ( %bitand( MATMIF_0001_info.Indicators : BIT_BOUND_HARDWARE_THREADS ) = BIT_BOUND_HARDWARE_THREADS );
          p_Bound_hardware_threads = 'YES';
        else;
          p_Bound_hardware_threads = 'NO';
        endif;

        if ( %bitand( MATMIF_0001_info.Indicators : BIT_DEDICATED_PROCESSORS ) = BIT_DEDICATED_PROCESSORS );
          p_Dedicated_processors = 'YES';
        else;
          p_Dedicated_processors = 'NO';
        endif;

        p_Maximum_processors_in_physical_machine                          = MATMIF_0001_info.Maximum_processors_in_physical_machine                       ;
        p_Minimum_virtual_processors                                      = MATMIF_0001_info.Minimum_virtual_processors                                   ;
        p_Maximum_virtual_processors                                      = MATMIF_0001_info.Maximum_virtual_processors                                   ;
        p_Minimum_processing_capacity                                     = MATMIF_0001_info.Minimum_processing_capacity                                  ;
        p_Maximum_processing_capacity                                     = MATMIF_0001_info.Maximum_processing_capacity                                  ;
        p_Processing_capacity_delta                                       = MATMIF_0001_info.Processing_capacity_delta                                    ;
        p_Minimum_interactive_capacity_percentage                         = MATMIF_0001_info.Minimum_interactive_capacity_percentage                      ;
        p_Maximum_interactive_capacity_percentage                         = MATMIF_0001_info.Maximum_interactive_capacity_percentage                      ;
        p_Hardware_threads_per_processor                                  = MATMIF_0001_info.Hardware_threads_per_processor                               ;

        // Convert from nul-terminated ASCII string to EBCDIC varchar.
        Partition_name_temp                                               = MATMIF_0001_info.Partition_name;
        p_Partition_name                                                  = %str( %addr( Partition_name_temp ) );

        select;
          when ( MATMIF_0001_info.Type_of_5250_measurement = 0 );
            p_Type_of_5250_measurement = 'CPW';
          when ( MATMIF_0001_info.Type_of_5250_measurement = 1 );
            p_Type_of_5250_measurement = 'USER';
          other;
            p_Type_of_5250_measurement = 'UNKNOWN';
        endsl;


        p_Memory_delta                                                    = MATMIF_0001_info.Memory_delta                                                 ;
        p_Configured_virtual_processors                                   = MATMIF_0001_info.Configured_virtual_processors                                ;
        p_Configured_processing_capacity                                  = MATMIF_0001_info.Configured_processing_capacity                               ;
        p_Configured_interactive_capacity_percentage                      = MATMIF_0001_info.Configured_interactive_capacity_percentage                   ;
        p_Configured_variable_processing_capacity_weight                  = MATMIF_0001_info.Configured_variable_processing_capacity_weight               ;
        p_Configured_memory                                               = MATMIF_0001_info.Configured_memory                                            ;
        p_Minimum_5250_OLTP_user_authorizations                           = MATMIF_0001_info.Minimum_5250_OLTP_user_authorizations                        ;
        p_Maximum_5250_OLTP_user_authorizations                           = MATMIF_0001_info.Maximum_5250_OLTP_user_authorizations                        ;
        p_Configured_5250_OLTP_user_authorizations                        = MATMIF_0001_info.Configured_5250_OLTP_user_authorizations                     ;

        Materialize_Machine_Information( MATMIF_0002_info : MATMIF_OPTION_2 );

        p_Usable_memory                                                   = MATMIF_0002_info.Usable_memory                                                ;
        p_CPU_time_since_IPL                                              = MATMIF_0002_info.CPU_time_since_IPL                                           ;
        p_Interactive_time_since_IPL                                      = MATMIF_0002_info.Interactive_time_since_IPL                                   ;
        p_Excess_interactive_time_since_IPL                               = MATMIF_0002_info.Excess_interactive_time_since_IPL                            ;
        p_Shared_processor_pool_idle_time_since_IPL                       = MATMIF_0002_info.Shared_processor_pool_idle_time_since_IPL                    ;


        if ( %bitand( MATMIF_0002_info.Indicators : BIT_SERVICE_AGGREGATION_POINT ) = BIT_SERVICE_AGGREGATION_POINT );
          p_Service_aggregation_point = 'YES';
        else;
          p_Service_aggregation_point = 'NO';
        endif;

        if ( %bitand( MATMIF_0002_info.Indicators : BIT_CAPPED_PARTITION ) = BIT_CAPPED_PARTITION );
          p_Capped_partition = 'YES';
        else;
          p_Capped_partition = 'NO';
        endif;

        if ( %bitand( MATMIF_0002_info.Indicators : BIT_HARDWARE_MULTITHREADING ) = BIT_HARDWARE_MULTITHREADING );
          p_Hardware_multithreading = 'YES';
        else;
          p_Hardware_multithreading = 'NO';
        endif;

        if ( %bitand( MATMIF_0002_info.Indicators : BIT_SHARED_PROCESSOR_POOL_IDLE_TIME_SINCE_IPL_VALID ) = BIT_SHARED_PROCESSOR_POOL_IDLE_TIME_SINCE_IPL_VALID );
          p_Shared_processor_pool_idle_time_since_IPL_valid = 'YES';
        else;
          p_Shared_processor_pool_idle_time_since_IPL_valid = 'NO';
        endif;

        p_Processors_in_the_physical_machine                              = MATMIF_0002_info.Processors_in_the_physical_machine                           ;
        p_Usable_virtual_processors                                       = MATMIF_0002_info.Usable_virtual_processors                                    ;
        p_Physical_processors_in_the_shared_processor_pool                = MATMIF_0002_info.Physical_processors_in_the_shared_processor_pool             ;
        p_Unallocated_processor_capacity_in_partition_group               = MATMIF_0002_info.Unallocated_processor_capacity_in_partition_group            ;
        p_Usable_processing_capacity                                      = MATMIF_0002_info.Usable_processing_capacity                                   ;
        p_Usable_variable_processing_capacity_weight                      = MATMIF_0002_info.Usable_variable_processing_capacity_weight                   ;
        p_Unallocated_variable_processing_capacity_weight                 = MATMIF_0002_info.Unallocated_variable_processing_capacity_weight              ;
        p_Minimum_required_processing_capacity                            = MATMIF_0002_info.Minimum_required_processing_capacity                         ;
        p_Interactive_capacity_percentage                                 = MATMIF_0002_info.Interactive_capacity_percentage                              ;
        p_Partition_group_ID                                              = MATMIF_0002_info.Partition_group_ID                                           ;
        p_Shared_processor_pool_ID                                        = MATMIF_0002_info.Shared_processor_pool_ID                                     ;
        p_Interactive_threshold                                           = MATMIF_0002_info.Interactive_threshold                                        ;
        p_Unallocated_interactive_processor_capacity_in_partition_group   = MATMIF_0002_info.Unallocated_interactive_processor_capacity_in_partition_group;
        p_Scaled_CPU_time_since_IPL                                       = MATMIF_0002_info.Scaled_CPU_time_since_IPL                                    ;
        p_Usable_5250_OLTP_user_authorization                             = MATMIF_0002_info.Usable_5250_OLTP_user_authorization                          ;
        p_Unallocated_5250_OLTP_user_authorization_in_partition_group     = MATMIF_0002_info.Unallocated_5250_OLTP_user_authorization_in_partition_group  ;
        p_Active_5250_users                                               = MATMIF_0002_info.Active_5250_users                                            ;

  endsl;

  SaveCallType = CallType;

  return;

  on-error *all;
    RcvMsg( ApiError.ExcpID : MsgText );
    SQL_State = '38999';
    MsgText = 'Error ' + ApiError.ExcpID + ': ' + MsgText + '. Please check joblog.';
    return;
    SQL_State = '38999';
    MsgText = 'Error occurred, please check joblog.';
    return;
  endmon;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    RcvMsg
// Description:  Receive message.

dcl-proc RcvMsg;
  dcl-pi *n ind;
    MsgID     char( 7 );
    MsgText   varchar( 1000 );
  end-pi;

  dcl-pr QMHRCVPM extpgm( 'QMHRCVPM' );
    *n likeds( RCVM0200 )             options( *varsize );           // Receiver variable
    *n int( 10 )                      const;                         // Length of receiver variable
    *n char(    8 )                   const;                         // Format name
    *n char(   10 )                   const;                         // Call stack entry
    *n int( 10 )                      const;                         // Call stack counter
    *n char(   10 )                   const;                         // Message type
    *n char(    4 )                   const;                         // Message key
    *n int( 10 )                      const;                         // Wait time
    *n char(   10 )                   const;                         // Message action
    *n char( 1024 )                   options( *varsize );           // Error code
  end-pr;

  dcl-ds RCVM0200 qualified len( 4096 );
    Bytes_provided   int( 10 ) inz( %size( RCVM0200 ) );
    Bytes_available  int( 10 );
    Message_ID       char( 7 ) pos( 13 );
    Message_data_len int( 10 ) pos( 153 );
    Message_text_len int( 10 ) pos( 161 );
    Message_data     char( 1 ) pos( 177 );
  end-ds;

  dcl-s  Message_text        char( %size( MsgText ) ) based( ptrMessage_text );
  dcl-s  ptrMessage_text     pointer;

  dcl-ds ERRC0100 likeds( ApiError_t );

  // Receive last message.

  QMHRCVPM( RCVM0200 : %size( RCVM0200 ) : 'RCVM0200' : '*' : 1 : '*LAST' : '' : 0 : '*SAME' : ERRC0100 );

  if ( ERRC0100.BytAvl = 0 );
    MsgID = RCVM0200.Message_ID;
    ptrMessage_text = %addr( RCVM0200.Message_data ) + RCVM0200.Message_data_len;
    MsgText = %subst( Message_text : 1 : %min( %size( MsgText ) : RCVM0200.Message_text_len ) );
  endif;

  return ( ERRC0100.BytAvl = 0 );

end-proc;
